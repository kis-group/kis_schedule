<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => 'ユーザーID', 'precision' => null, 'unsigned' => null],
        'username' => ['type' => 'string', 'length' => null, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'ユーザー名称', 'precision' => null],
        'password' => ['type' => 'string', 'length' => null, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'パスワード', 'precision' => null],
        'role_id' => ['type' => 'integer', 'length' => 10, 'default' => '0', 'null' => false, 'comment' => '権限ID', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'updated_at' => ['type' => 'timestamptimezone', 'length' => null, 'default' => 'now()', 'null' => true, 'comment' => '更新日時', 'precision' => 6],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'username' => 'Lorem ipsum dolor sit amet',
                'password' => 'Lorem ipsum dolor sit amet',
                'role_id' => 1,
                'updated_at' => 1636598660,
            ],
        ];
        parent::init();
    }
}
