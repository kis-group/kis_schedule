<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReserveStatusTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReserveStatusTable Test Case
 */
class ReserveStatusTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ReserveStatusTable
     */
    protected $ReserveStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ReserveStatus',
        'app.Aircons',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ReserveStatus') ? [] : ['className' => ReserveStatusTable::class];
        $this->ReserveStatus = $this->getTableLocator()->get('ReserveStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ReserveStatus);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ReserveStatusTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
