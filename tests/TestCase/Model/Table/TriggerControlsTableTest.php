<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TriggerControlsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TriggerControlsTable Test Case
 */
class TriggerControlsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TriggerControlsTable
     */
    protected $TriggerControls;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.TriggerControls',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('TriggerControls') ? [] : ['className' => TriggerControlsTable::class];
        $this->TriggerControls = $this->getTableLocator()->get('TriggerControls', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TriggerControls);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\TriggerControlsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
