<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AirconsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AirconsTable Test Case
 */
class AirconsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AirconsTable
     */
    protected $Aircons;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Aircons',
        'app.ReserveStatus',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Aircons') ? [] : ['className' => AirconsTable::class];
        $this->Aircons = $this->getTableLocator()->get('Aircons', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Aircons);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\AirconsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\AirconsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
