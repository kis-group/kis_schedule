<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReserveStatus Model
 *
 * @property \App\Model\Table\AirconsTable&\Cake\ORM\Association\HasMany $Aircons
 *
 * @method \App\Model\Entity\ReserveStatus newEmptyEntity()
 * @method \App\Model\Entity\ReserveStatus newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ReserveStatus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReserveStatus get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReserveStatus findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ReserveStatus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReserveStatus[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReserveStatus|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReserveStatus saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReserveStatus[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ReserveStatus[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ReserveStatus[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ReserveStatus[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ReserveStatusTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('reserve_status');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Aircons', [
            'foreignKey' => 'reserve_status_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->allowEmptyString('name');

        $validator
            ->scalar('hex_color_code')
            ->maxLength('hex_color_code', 7)
            ->notEmptyString('hex_color_code');

        return $validator;
    }
}
