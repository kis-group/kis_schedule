<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\FrozenTime;
use Cake\Core\Configure;

/**
 * Aircons Model
 *
 * @property \App\Model\Table\ReserveStatusTable&\Cake\ORM\Association\BelongsTo $ReserveStatus
 *
 * @method \App\Model\Entity\Aircon newEmptyEntity()
 * @method \App\Model\Entity\Aircon newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Aircon[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Aircon get($primaryKey, $options = [])
 * @method \App\Model\Entity\Aircon findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Aircon patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Aircon[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Aircon|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Aircon saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Aircon[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Aircon[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Aircon[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Aircon[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class AirconsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('aircons');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ReserveStatus', [
            'foreignKey' => 'reserve_status_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('date_at')
            ->requirePresence('date_at', 'create')
            ->notEmptyDate('date_at');

        $validator
            ->boolean('morning_flg')
            ->notEmptyString('morning_flg');

        $validator
            ->scalar('remark')
            ->allowEmptyString('remark');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['reserve_status_id'], 'ReserveStatus'), ['errorField' => 'reserve_status_id']);

        return $rules;
    }

    /**
     * 予約状況の一覧を取得します。
     */
    public function findMonthly(array $options = array())
    {
        $now = date("Y-m-d");
        $target_begin = date("Y-m-d", strtotime($now.'+ 3 days'));
        $target_end = date("Y-m-d", strtotime($now.'+ 2 months'));

        // エイリアス名はフロントエンドライブラリの都合による
        $result = $this->query()
        ->select([
            'id',
            'startdate' => 'date_at',
            'enddate' => "''",
            'morning_flg' => 'aircons.morning_flg',
            'name' => "''",
            'color' => 'a.hex_color_code',
            'status' => 'reserve_status_id',
        ])
        ->join([
            'table' => 'reserve_status',
            'alias' => 'a',
            'conditions' => 'aircons.reserve_status_id = a.id',
        ])
        ->where(function($exp) use ($target_begin, $target_end) {
            return $exp->between('aircons.date_at', $target_begin, $target_end);
        })
        ->order([
            'aircons.date_at',
            'aircons.morning_flg' => 'DESC',
        ])
        ->toArray();

        $row_number = 1;
        foreach ($result as &$row) {

            $status_str = $row->status === Configure::read('reserve.status.full')
                ? Configure::read("reserve.status_display.{$row->status}")
                : "";

            // ### 初日の表示 ###

            // 必ず 「午前 or 午後」 を先頭につける
            if ($row_number === 1) {
                $row->name = "午前 {$status_str}";
                $row_number++;
                continue;
            }
            else if ($row_number === 2) {
                $row->name = "午後 {$status_str}";
                $row_number++;
                continue;
            }
            // else

            // ### ２日目以降の表示 ###
            // × -> ×
            if ($row->status === Configure::read('reserve.status.full')) {
                $row->name = Configure::read("reserve.status_display.{$row->status}");
                $row_number++;
                continue;
            }

            // △ -> 午前 or 午後
            // ○ -> 午前 or 午後
            if ($row->morning_flg === true) {
                $row->name = "午前 {$status_str}";
            }
            else {
                $row->name = "午後 {$status_str}";
            }

            $row_number++;
        }

        return $result;
    }
}
