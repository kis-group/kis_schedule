<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TriggerControls Model
 *
 * @method \App\Model\Entity\TriggerControl newEmptyEntity()
 * @method \App\Model\Entity\TriggerControl newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\TriggerControl[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TriggerControl get($primaryKey, $options = [])
 * @method \App\Model\Entity\TriggerControl findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\TriggerControl patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TriggerControl[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\TriggerControl|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TriggerControl saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TriggerControl[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TriggerControl[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\TriggerControl[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TriggerControl[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class TriggerControlsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('trigger_controls');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('value')
            ->notEmptyString('value');

        $validator
            ->boolean('is_end')
            ->notEmptyString('is_end');

        $validator
            ->dateTime('updated_at')
            ->notEmptyDateTime('updated_at');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        return $validator;
    }

    /**
     * 未実行タスクを取得します。
     */
    public function findPenddings(array $options = array())
    {
        $result = $this->query()
        ->select([
            'id',
        ])
        ->where([
            'value' => $options['value'],
            'is_end' => $options['is_end'],
        ])
        ->toArray();

        return $result;
    }

    /**
     * 実行済タスクに更新します。
     */
    public function updateToEnd(array $options = array())
    {
        $this->query()->update()
                ->set(['is_end' => true])
                ->where(['value' => $options['value']])
                ->execute();
    }
}
