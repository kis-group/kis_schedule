<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Aircon Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $date_at
 * @property bool $morning_flg
 * @property int $reserve_status_id
 * @property string|null $remark
 *
 * @property \App\Model\Entity\ReserveStatus $reserve_status
 */
class Aircon extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'date_at' => true,
        'morning_flg' => true,
        'reserve_status_id' => true,
        'remark' => true,
        'reserve_status' => true,
    ];
}
