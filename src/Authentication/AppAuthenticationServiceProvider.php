<?php
declare(strict_types=1);

namespace App\Authentication;

use Authentication\AuthenticationService;
use Authentication\AuthenticationServiceInterface;
use Authentication\AuthenticationServiceProviderInterface;
use Authentication\Identifier\IdentifierInterface;
use Authentication\Middleware\AuthenticationMiddleware;
use Cake\Http\MiddlewareQueue;
use Cake\Routing\Router;
use Psr\Http\Message\ServerRequestInterface;

class AppAuthenticationServiceProvider implements AuthenticationServiceProviderInterface
{
    public function getAuthenticationService(ServerRequestInterface $request): AuthenticationServiceInterface
    {
        $service = new AuthenticationService();

        $service->setConfig([
            'unauthenticatedRedirect' => Router::url([
                'prefix' => false,
                'plugin' => null,
                'controller' => 'Users',
                'action' => 'login']),
                'queryParam' => 'redirect',
        ]);


        $fields = [
            IdentifierInterface::CREDENTIAL_USERNAME => 'username',
            IdentifierInterface::CREDENTIAL_PASSWORD => 'password'
        ];

        /*
        ユーザーデータのセッションキー、 デフォルトは Auth.
        AuthはMemberでのログインで使用している為、セッションキーを変更する
        */
        $service->loadAuthenticator('Authentication.Session',[
            'sessionKey' => 'AdminAuth',
        ]);
        // $service->loadAuthenticator('Authentication.Session');

        $service->loadAuthenticator('Authentication.Form',[
                'fields' => $fields,
                'loginUrl' => Router::url([
                    'prefix' => false,
                    'plugin' => null,
                    'controller' => 'Users',
                    'action' => 'login'
                ]),
        ]);

        $service->loadIdentifier('Authentication.Password',[
            'resolver'=>[                               
                'className' => 'Authentication.Orm',    
                'userModel' => 'Users'                   
            ],                                          
            'fields' => [
                'username' => 'username',
                'password' => 'password',
            ]
        ]);

        

        // // 認証者を読み込みます。セッションを優先してください。
        // $service->loadAuthenticator('Authentication.Session');
        // $service->loadAuthenticator('Authentication.Form', [
        //    'fields' => $fields,
        //    'loginUrl' => '/users/login'
        // ]);

        // // 識別子を読み込みます。
        // $service->loadIdentifier('Authentication.Password', compact('fields'));

        return $service;
    }
}