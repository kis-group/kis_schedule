<?php
declare(strict_types=1);

namespace App\Service;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;

/**
 * @property \App\Model\Table\TriggerControlsTable $TriggerControls
 */
class PollingService extends AppService {

    public function getIsPenddings() {

        $penddings = $this->TriggerControls->findPenddings([
            // git push 検知
            'value' => '1',
            'is_end' => false,
        ]);

        if (count($penddings) > 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
}
