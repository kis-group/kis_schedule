<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ReserveStatus Controller
 *
 * @property \App\Model\Table\ReserveStatusTable $ReserveStatus
 * @method \App\Model\Entity\ReserveStatus[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReserveStatusController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $reserveStatus = $this->paginate($this->ReserveStatus);

        $this->set(compact('reserveStatus'));
    }

    /**
     * View method
     *
     * @param string|null $id Reserve Status id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reserveStatus = $this->ReserveStatus->get($id, [
            'contain' => ['Aircons'],
        ]);

        $this->set(compact('reserveStatus'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reserveStatus = $this->ReserveStatus->newEmptyEntity();
        if ($this->request->is('post')) {
            $reserveStatus = $this->ReserveStatus->patchEntity($reserveStatus, $this->request->getData());
            if ($this->ReserveStatus->save($reserveStatus)) {
                $this->Flash->success(__('The reserve status has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The reserve status could not be saved. Please, try again.'));
        }
        $this->set(compact('reserveStatus'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Reserve Status id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reserveStatus = $this->ReserveStatus->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reserveStatus = $this->ReserveStatus->patchEntity($reserveStatus, $this->request->getData());
            if ($this->ReserveStatus->save($reserveStatus)) {
                $this->Flash->success(__('The reserve status has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The reserve status could not be saved. Please, try again.'));
        }
        $this->set(compact('reserveStatus'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Reserve Status id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reserveStatus = $this->ReserveStatus->get($id);
        if ($this->ReserveStatus->delete($reserveStatus)) {
            $this->Flash->success(__('The reserve status has been deleted.'));
        } else {
            $this->Flash->error(__('The reserve status could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
