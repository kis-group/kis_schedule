<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\PollingService;

/**
 * TriggerControls Controller
 *
 * @property \App\Model\Table\TriggerControlsTable $TriggerControls
 * @method \App\Model\Entity\TriggerControl[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TriggerControlsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $triggerControls = $this->paginate($this->TriggerControls);

        $this->set(compact('triggerControls'));
    }

    /**
     * View method
     *
     * @param string|null $id Trigger Control id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $triggerControl = $this->TriggerControls->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('triggerControl'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $triggerControl = $this->TriggerControls->newEmptyEntity();
        if ($this->request->is('post')) {
            $triggerControl = $this->TriggerControls->patchEntity($triggerControl, $this->request->getData());
            if ($this->TriggerControls->save($triggerControl)) {
                $this->Flash->success(__('The trigger control has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The trigger control could not be saved. Please, try again.'));
        }
        $this->set(compact('triggerControl'));
    }

    public function append($value)
    {
        $this->autoRender = false;

        $triggerControl = $this->TriggerControls->newEmptyEntity();

        $triggerControl = $this->TriggerControls->patchEntity($triggerControl, []);
        $triggerControl["value"] = $value;

        if ($this->TriggerControls->save($triggerControl)) {
        }
        else {
        }
    }

    public function updateToEnd($value)
    {
        $this->autoRender = false;
        $this->TriggerControls->updateToEnd([
            'value' => $value,
        ]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Trigger Control id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $triggerControl = $this->TriggerControls->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $triggerControl = $this->TriggerControls->patchEntity($triggerControl, $this->request->getData());
            if ($this->TriggerControls->save($triggerControl)) {
                $this->Flash->success(__('The trigger control has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The trigger control could not be saved. Please, try again.'));
        }
        $this->set(compact('triggerControl'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Trigger Control id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $triggerControl = $this->TriggerControls->get($id);
        if ($this->TriggerControls->delete($triggerControl)) {
            $this->Flash->success(__('The trigger control has been deleted.'));
        } else {
            $this->Flash->error(__('The trigger control could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function getPenddings()
    {
        $this->viewBuilder()->setClassName('Json');

        $pollingService = new PollingService();

        try {
            $result = $pollingService->getIsPenddings();

            $this->set('exist_penddings', $result);
            $this->viewBuilder()->setOption('serialize', ['exist_penddings']);

        } catch (\Throwable $th) {
        }
    }
}
