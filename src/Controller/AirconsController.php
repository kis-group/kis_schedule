<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;

/**
 * Aircons Controller
 *
 * @property \App\Model\Table\AirconsTable $Aircons
 * @method \App\Model\Entity\Aircon[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AirconsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ReserveStatus'],
            "order" => [
                "date_at" => "ASC",
                "morning_flg" => "DESC",
            ],
            'limit' => 60,
        ];

        $target_begin = date("Y-m-d");
        $target_end = date("Y-m-d", strtotime($target_begin.'+ 6 months'));

        $aircons = $this->Aircons->find()
            ->where(function($exp) use ($target_begin, $target_end) {
                return $exp->between('aircons.date_at', $target_begin, $target_end);
            });

        $this->paginate($aircons);

        $p = file_get_contents("./p.txt");
        $this->set(compact('aircons', 'p'));
    }

    /**
     * View method
     *
     * @param string|null $id Aircon id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $aircon = $this->Aircons->get($id, [
            'contain' => ['ReserveStatus'],
        ]);

        $this->set(compact('aircon'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $aircon = $this->Aircons->newEmptyEntity();
        if ($this->request->is('post')) {
            $aircon = $this->Aircons->patchEntity($aircon, $this->request->getData());
            if ($this->Aircons->save($aircon)) {
                $this->Flash->success(__('The aircon has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The aircon could not be saved. Please, try again.'));
        }
        $reserveStatus = $this->Aircons->ReserveStatus->find('list', ['limit' => 200]);
        $this->set(compact('aircon', 'reserveStatus'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Aircon id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($page_number = 1)
    {
        $fileContent = $this->request->getData('fffff');

        if (file_put_contents('./p.txt', $fileContent) !== false) {
            $this->Flash->success('更新に成功しました。');
        } else {
            $this->Flash->error('更新に失敗しました。');
        }

        return $this->redirect(['action' => 'index?page=' . $page_number]);
    }

    public function update($id = null, $status = null, $page_number = 1) {

        // $this->autoRender = false;

        // todo: GET -> POST
        $aircon = $this->Aircons->get($id, [
            'contain' => [],
        ]);

        $aircon = $this->Aircons->patchEntity($aircon, $this->request->getData());
        $aircon["reserve_status_id"] = $status;

        if ($this->Aircons->save($aircon)) {

            $youbi = DATE_JP[date('w', strtotime($aircon['date_at']->format('Y-m-d')))];
            $ampm = $aircon['morning_flg'] == 1 ? "午前" : "午後";

            $this->Flash->success($aircon['date_at']->format('Y-m-d') . "({$youbi}) {$ampm} " . 'の予約状況を更新しました。');

            return $this->redirect(['action' => 'index?page=' . $page_number]);
        }

        $this->Flash->error(__('The aircon could not be saved. Please, try again.'));

        $reserveStatus = $this->Aircons->ReserveStatus->find('list', ['limit' => 200]);
        $this->set(compact('aircon', 'reserveStatus'));

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Aircon id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $aircon = $this->Aircons->get($id);
        if ($this->Aircons->delete($aircon)) {
            $this->Flash->success(__('The aircon has been deleted.'));
        } else {
            $this->Flash->error(__('The aircon could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function getMonthly()
    {
        $this->loadModel('Aircons');
        $aircons = $this->Aircons->findMonthly();
        $this->viewBuilder()->setClassName('Json');
        $this->set('monthly', $aircons);
        $this->viewBuilder()->setOption('serialize', ['monthly']);
    }
}
