<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ScheduleSettings Controller
 *
 * @property \App\Model\Table\ScheduleSettingsTable $ScheduleSettings
 * @method \App\Model\Entity\ScheduleSetting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ScheduleSettingsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $scheduleSettings = $this->paginate($this->ScheduleSettings);

        $this->set(compact('scheduleSettings'));
    }

    /**
     * View method
     *
     * @param string|null $id Schedule Setting id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $scheduleSetting = $this->ScheduleSettings->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('scheduleSetting'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $scheduleSetting = $this->ScheduleSettings->newEmptyEntity();
        if ($this->request->is('post')) {
            $scheduleSetting = $this->ScheduleSettings->patchEntity($scheduleSetting, $this->request->getData());
            if ($this->ScheduleSettings->save($scheduleSetting)) {
                $this->Flash->success(__('The schedule setting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The schedule setting could not be saved. Please, try again.'));
        }
        $this->set(compact('scheduleSetting'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Schedule Setting id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $scheduleSetting = $this->ScheduleSettings->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $scheduleSetting = $this->ScheduleSettings->patchEntity($scheduleSetting, $this->request->getData());
            if ($this->ScheduleSettings->save($scheduleSetting)) {
                $this->Flash->success(__('The schedule setting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The schedule setting could not be saved. Please, try again.'));
        }
        $this->set(compact('scheduleSetting'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Schedule Setting id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $scheduleSetting = $this->ScheduleSettings->get($id);
        if ($this->ScheduleSettings->delete($scheduleSetting)) {
            $this->Flash->success(__('The schedule setting has been deleted.'));
        } else {
            $this->Flash->error(__('The schedule setting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
