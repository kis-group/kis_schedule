<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use App\Service\PollingService;

class DatabaseCommand extends Command
{
    public function penddings(Arguments $args, ConsoleIo $io)
    {
        $pollingService = new PollingService();

        try {
            $result = $pollingService->getIsPenddings();
            $io->out('Hello world.' . $result);
        } catch (\Throwable $th) {
            $io->error('it is error.' . $th);
            $this->abort();
        }
    }
}
