<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ScheduleSetting[]|\Cake\Collection\CollectionInterface $scheduleSettings
 */
?>
<div class="scheduleSettings index content">
    <?= $this->Html->link(__('New Schedule Setting'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Schedule Settings') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('title_text') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($scheduleSettings as $scheduleSetting): ?>
                <tr>
                    <td><?= $this->Number->format($scheduleSetting->id) ?></td>
                    <td><?= h($scheduleSetting->title_text) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $scheduleSetting->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $scheduleSetting->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $scheduleSetting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $scheduleSetting->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
