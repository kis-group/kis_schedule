<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ScheduleSetting $scheduleSetting
 */
?>
<div class="row">
    <div class="column-responsive column-80">
        <h3><?= __('表示設定') ?></h3>
        <?= $this->Html->link(__('予約状況登録画面'), ['controller'=>'aircons', 'action'=>''], ['class'=> 'button']) ?>
        <div class="scheduleSettings form content">
            <?= $this->Form->create($scheduleSetting) ?>
            <fieldset>
                <?php
                    echo $this->Form->control('title_text', ['label' => 'タイトル：お掃除kis ●●● の施工可能日']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('更新')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
