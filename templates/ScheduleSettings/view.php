<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ScheduleSetting $scheduleSetting
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Schedule Setting'), ['action' => 'edit', $scheduleSetting->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Schedule Setting'), ['action' => 'delete', $scheduleSetting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $scheduleSetting->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Schedule Settings'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Schedule Setting'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="scheduleSettings view content">
            <h3><?= h($scheduleSetting->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Title Text') ?></th>
                    <td><?= h($scheduleSetting->title_text) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($scheduleSetting->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
