<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ScheduleSetting $scheduleSetting
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Schedule Settings'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="scheduleSettings form content">
            <?= $this->Form->create($scheduleSetting) ?>
            <fieldset>
                <legend><?= __('Add Schedule Setting') ?></legend>
                <?php
                    echo $this->Form->control('title_text');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
