<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReserveStatus $reserveStatus
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Reserve Status'), ['action' => 'edit', $reserveStatus->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Reserve Status'), ['action' => 'delete', $reserveStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reserveStatus->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Reserve Status'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Reserve Status'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="reserveStatus view content">
            <h3><?= h($reserveStatus->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($reserveStatus->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Hex Color Code') ?></th>
                    <td><?= h($reserveStatus->hex_color_code) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($reserveStatus->id) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Aircons') ?></h4>
                <?php if (!empty($reserveStatus->aircons)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Date At') ?></th>
                            <th><?= __('Morning Flg') ?></th>
                            <th><?= __('Reserve Status Id') ?></th>
                            <th><?= __('Remark') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($reserveStatus->aircons as $aircons) : ?>
                        <tr>
                            <td><?= h($aircons->id) ?></td>
                            <td><?= h($aircons->date_at) ?></td>
                            <td><?= h($aircons->morning_flg) ?></td>
                            <td><?= h($aircons->reserve_status_id) ?></td>
                            <td><?= h($aircons->remark) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Aircons', 'action' => 'view', $aircons->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Aircons', 'action' => 'edit', $aircons->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Aircons', 'action' => 'delete', $aircons->id], ['confirm' => __('Are you sure you want to delete # {0}?', $aircons->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
