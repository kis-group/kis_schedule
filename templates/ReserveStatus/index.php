<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReserveStatus[]|\Cake\Collection\CollectionInterface $reserveStatus
 */
?>
<div class="reserveStatus index content">
    <?= $this->Html->link(__('New Reserve Status'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Reserve Status') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('hex_color_code') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($reserveStatus as $reserveStatus): ?>
                <tr>
                    <td><?= $this->Number->format($reserveStatus->id) ?></td>
                    <td><?= h($reserveStatus->name) ?></td>
                    <td><?= h($reserveStatus->hex_color_code) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $reserveStatus->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $reserveStatus->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $reserveStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reserveStatus->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
