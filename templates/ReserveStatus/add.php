<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReserveStatus $reserveStatus
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Reserve Status'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="reserveStatus form content">
            <?= $this->Form->create($reserveStatus) ?>
            <fieldset>
                <legend><?= __('Add Reserve Status') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('hex_color_code');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
