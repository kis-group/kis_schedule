<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TriggerControl $triggerControl
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Trigger Control'), ['action' => 'edit', $triggerControl->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Trigger Control'), ['action' => 'delete', $triggerControl->id], ['confirm' => __('Are you sure you want to delete # {0}?', $triggerControl->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Trigger Controls'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Trigger Control'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="triggerControls view content">
            <h3><?= h($triggerControl->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($triggerControl->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Value') ?></th>
                    <td><?= $this->Number->format($triggerControl->value) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated At') ?></th>
                    <td><?= h($triggerControl->updated_at) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created At') ?></th>
                    <td><?= h($triggerControl->created_at) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is End') ?></th>
                    <td><?= $triggerControl->is_end ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
