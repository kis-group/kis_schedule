<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TriggerControl $triggerControl
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Trigger Controls'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="triggerControls form content">
            <?= $this->Form->create($triggerControl) ?>
            <fieldset>
                <legend><?= __('Add Trigger Control') ?></legend>
                <?php
                    echo $this->Form->control('value');
                    echo $this->Form->control('is_end');
                    echo $this->Form->control('updated_at');
                    echo $this->Form->control('created_at');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
