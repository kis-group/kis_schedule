<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TriggerControl[]|\Cake\Collection\CollectionInterface $triggerControls
 */
?>
<div class="triggerControls index content">
    <?= $this->Html->link(__('New Trigger Control'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Trigger Controls') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('value') ?></th>
                    <th><?= $this->Paginator->sort('is_end') ?></th>
                    <th><?= $this->Paginator->sort('updated_at') ?></th>
                    <th><?= $this->Paginator->sort('created_at') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($triggerControls as $triggerControl): ?>
                <tr>
                    <td><?= $this->Number->format($triggerControl->id) ?></td>
                    <td><?= $this->Number->format($triggerControl->value) ?></td>
                    <td><?= h($triggerControl->is_end) ?></td>
                    <td><?= h($triggerControl->updated_at) ?></td>
                    <td><?= h($triggerControl->created_at) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $triggerControl->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $triggerControl->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $triggerControl->id], ['confirm' => __('Are you sure you want to delete # {0}?', $triggerControl->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
