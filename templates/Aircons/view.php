<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Aircon $aircon
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Aircon'), ['action' => 'edit', $aircon->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Aircon'), ['action' => 'delete', $aircon->id], ['confirm' => __('Are you sure you want to delete # {0}?', $aircon->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Aircons'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Aircon'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="aircons view content">
            <h3><?= h($aircon->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Reserve Status') ?></th>
                    <td><?= $aircon->has('reserve_status') ? $this->Html->link($aircon->reserve_status->name, ['controller' => 'ReserveStatus', 'action' => 'view', $aircon->reserve_status->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Remark') ?></th>
                    <td><?= h($aircon->remark) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($aircon->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Date At') ?></th>
                    <td><?= h($aircon->date_at) ?></td>
                </tr>
                <tr>
                    <th><?= __('Morning Flg') ?></th>
                    <td><?= $aircon->morning_flg ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
