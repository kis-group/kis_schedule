<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Aircon $aircon
 * @var string[]|\Cake\Collection\CollectionInterface $reserveStatus
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $aircon->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $aircon->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Aircons'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="aircons form content">
            <?= $this->Form->create($aircon) ?>
            <fieldset>
                <legend><?= __('Edit Aircon') ?></legend>
                <?php
                    echo $this->Form->control('date_at');
                    echo $this->Form->control('morning_flg');
                    echo $this->Form->control('reserve_status_id', ['options' => $reserveStatus]);
                    echo $this->Form->control('remark');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
