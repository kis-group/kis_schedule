<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Aircon[]|\Cake\Collection\CollectionInterface $aircons
 */
?>
<style>
table td {
	background: #eee;
}
table tr:nth-child(3) td { background: #fff; }
table tr:nth-child(4) td { background: #fff; }
table tr:nth-child(7) td { background: #fff; }
table tr:nth-child(8) td { background: #fff; }
table tr:nth-child(11) td { background: #fff; }
table tr:nth-child(12) td { background: #fff; }
table tr:nth-child(15) td { background: #fff; }
table tr:nth-child(16) td { background: #fff; }
table tr:nth-child(19) td { background: #fff; }
table tr:nth-child(20) td { background: #fff; }
table tr:nth-child(23) td { background: #fff; }
table tr:nth-child(24) td { background: #fff; }
table tr:nth-child(27) td { background: #fff; }
table tr:nth-child(28) td { background: #fff; }
table tr:nth-child(31) td { background: #fff; }
table tr:nth-child(32) td { background: #fff; }
table tr:nth-child(35) td { background: #fff; }
table tr:nth-child(36) td { background: #fff; }
table tr:nth-child(39) td { background: #fff; }
table tr:nth-child(40) td { background: #fff; }
table tr:nth-child(43) td { background: #fff; }
table tr:nth-child(44) td { background: #fff; }
table tr:nth-child(47) td { background: #fff; }
table tr:nth-child(48) td { background: #fff; }
table tr:nth-child(51) td { background: #fff; }
table tr:nth-child(52) td { background: #fff; }
table tr:nth-child(55) td { background: #fff; }
table tr:nth-child(56) td { background: #fff; }
table tr:nth-child(59) td { background: #fff; }
table tr:nth-child(60) td { background: #fff; }
table tr:nth-child(63) td { background: #fff; }
table tr:nth-child(64) td { background: #fff; }
table tr:nth-child(67) td { background: #fff; }
table tr:nth-child(68) td { background: #fff; }
table tr:nth-child(71) td { background: #fff; }
table tr:nth-child(72) td { background: #fff; }
table tr:nth-child(75) td { background: #fff; }
table tr:nth-child(76) td { background: #fff; }
table tr:nth-child(79) td { background: #fff; }
table tr:nth-child(80) td { background: #fff; }
table tr:nth-child(83) td { background: #fff; }
table tr:nth-child(84) td { background: #fff; }
table tr:nth-child(87) td { background: #fff; }
table tr:nth-child(88) td { background: #fff; }
table tr:nth-child(91) td { background: #fff; }
table tr:nth-child(92) td { background: #fff; }
table tr:nth-child(95) td { background: #fff; }
table tr:nth-child(96) td { background: #fff; }
table tr:nth-child(99) td { background: #fff; }
table tr:nth-child(100) td { background: #fff; }

</style>
<div class="aircons index content">
    <h3><?= __('エアコンクリーニング空き状況 管理画面📆') ?></h3>

    <?= $this->Form->create(null, ['url' => ['controller' => 'aircons', 'action' => 'edit']]); ?>
    <?= $this->Form->text('fffff', ['value' => $p]); ?>
    <?= $this->Form->submit('kisftp[xx] 更新', ['class' => 'button']); ?>
    <?= $this->Form->end(); ?>

    <?php if (false) { ?>
        <?= $this->Html->link(__('表示設定'), ['controller'=>'schedule_settings', 'action'=>'edit', 1], ['class'=> 'button']) ?>
    <?php } ?>

    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= 'showing from ' . h($aircons->first()->date_at->format('Y-m-d')) . ' to ' . h($aircons->last()->date_at->format('Y-m-d')) ?></p>
    </div>

    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('日付') ?></th>
                    <th><?= $this->Paginator->sort('午前・午後') ?></th>
                    <th><?= $this->Paginator->sort('予約状況') ?></th>
                    <th class="actions"><?= __('更新') ?></th>
                </tr>
            </thead>
            <tbody>

                <?php $row_index = 1 ?>
                <?php foreach ($aircons as $aircon): ?>
                <tr>
                <?php if ($row_index % 2 == 1) { ?>
                        <td><?= h($aircon->date_at->format('Y-m-d')) . ' ' . '(' . h(DATE_JP[date('w', strtotime($aircon->date_at->format('Y-m-d')))]) . ')' ?></td>
                    <?php } else { ?>
                        <td></td>
                    <?php } ?>
                    <?php $row_index = $row_index + 1 ?>
                    <td><?= $aircon->morning_flg === true ? '午前' : '午後'; ?></td>
                    <?php if ($aircon->reserve_status->id === 0): ?>
                        <td><?= $aircon->has('reserve_status') ? $this->Html->div('button schedule_status3 view_status', "　　　") : '' ?></td>
                    <?php elseif ($aircon->reserve_status->id === 1): ?>
                        <td><?= $aircon->has('reserve_status') ? $this->Html->div('button schedule_status2 view_status', "　　　") : '' ?></td>
                    <?php else: ?>
                        <td><?= $aircon->has('reserve_status') ? $this->Html->div('button schedule_status1 view_status', "　　　") : '' ?></td>
                    <?php endif; ?>
                    <td class="actions">
                        <?= $this->Form->postLink(__('予約可'), ['action' => 'update', $aircon->id, 0, $this->Paginator->current()], ['class' => 'button schedule_status3']) ?>
                        <?= $this->Form->postLink(__('残りわずか'), ['action' => 'update', $aircon->id, 1, $this->Paginator->current()], ['class' => 'button schedule_status2']) ?>
                        <?= $this->Form->postLink(__('予約不可'), ['action' => 'update', $aircon->id, 2, $this->Paginator->current()], ['class' => 'button schedule_status1']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
    </div>
</div>
