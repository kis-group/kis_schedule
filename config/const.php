<?php

define('DATE_JP', ['日', '月', '火', '水', '木', '金', '土']);

// Configureの場合
return [
    'reserve' => [
        'status' => [
            'available' => 0,
            'almost_full' => 1,
            'full' => 2
        ],
        'status_display' => [
            '0' => '○',
            '1' => '△',
            '2' => '✕',
        ],
    ],
];
